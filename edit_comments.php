<?php
    session_start();
    if(isset($_POST['id'])){
       $id = isset($_POST['id']) ? filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING) : '';
       $changed_comment = isset($_POST['changed_comment']) ? filter_input(INPUT_POST, 'changed_comment', FILTER_SANITIZE_STRING) : '';
       $username = $_SESSION['username'];

       if($_SESSION['token'] !== $_POST['token']){
             die("Request forgery detected");
        } 
       require 'database.php';

        



       $stmt2 = $mysqli->prepare("UPDATE comments SET comments = ? WHERE comment_id = ?");
       if(!$stmt2){
         printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }
       $stmt2->bind_param('ss', $changed_comment, $id);
       $stmt2->execute();
       $stmt2->close();



       header("Location: stories.php");
       exit;
      }

?>