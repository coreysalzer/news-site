<?php
    session_start();
    if(isset($_POST['id'])){
       $id = isset($_POST['delete_id']) ? filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING) : '';

       if($_SESSION['token'] !== $_POST['token']){
             die("Request forgery detected");
        } 
       require 'database.php';

    
    


       $stmt2 = $mysqli->prepare("DELETE FROM comments WHERE comments.comment_id = ?");
       if(!$stmt2){
         printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }
       $stmt2->bind_param('s', $id);
       $stmt2->execute();
       $stmt2->close();



       header("Location: stories.php");
       exit;
      }
    header("Location: stories.php");
?>