<?php
    session_start();
    if(isset($_POST['storytodelete'])){
      
       $story_name = isset($_POST['storytodelete']) ? filter_input(INPUT_POST, 'storytodelete', FILTER_SANITIZE_STRING) : '';

       if($_SESSION['token'] !== $_POST['token']){
             die("Request forgery detected");
        } 
       require 'database.php';
       
       $stmt1 = $mysqli->prepare("DELETE FROM stories WHERE name=?");
       if(!$stmt1){
         printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }
       $stmt1->bind_param('s', $story_name);
       $stmt1->execute();
       $stmt1->close();
        
       header("Location: stories.php");
       exit;
      }
?>