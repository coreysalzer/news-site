<?php
    session_start();
    printf(isset($_POST['storyname']));
    if(isset($_POST['storyname'])){
      printf("I ran!");
       $story_name = isset($_POST['storyname']) ? filter_input(INPUT_POST, 'storyname', FILTER_SANITIZE_STRING) : '';
       $story_name_edit = isset($_POST['storyname_edit']) ? filter_input(INPUT_POST, 'storyname_edit', FILTER_SANITIZE_STRING) : '';
       $link = isset($_POST['link']) ? filter_input(INPUT_POST, 'link', FILTER_SANITIZE_STRING) : '';
       $comment = isset($_POST['comment']) ? filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_STRING) : '';
       $username = $_SESSION['username'];

       if($_SESSION['token'] !== $_POST['token']){
             die("Request forgery detected");
        } 
       require 'database.php';

       printf("I ran!");

       
       $stmt1 = $mysqli->prepare("SELECT link, commentary, username FROM stories WHERE name = ? ");
       if(!$stmt1){
         printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }
       $stmt1->bind_param('s', $story_name);
       $stmt1->execute();
       $stmt1->bind_result($original_link, $original_comment,$user);
       $stmt1->fetch();
       $stmt1->close();

       if ($user !== $username){
        printf("You did not post this story!");
        exit;
       }


       if($link == ""){
          $link = $original_link;
       }
       if($story_name_edit == ""){
          $story_name_edit = $story_name;
       }
       if($comment == ""){
          $comment = $original_comment;
       }

       $stmt2 = $mysqli->prepare("UPDATE stories SET name = ?, link = ? , commentary = ? WHERE name = ?");
       if(!$stmt2){
         printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }
       $stmt2->bind_param('ssss',$story_name_edit, $link, $comment, $story_name);
       $stmt2->execute();
       $stmt2->close();



       header("Location: stories.php");
       exit;
      }

?>