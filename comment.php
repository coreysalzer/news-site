<?php
    session_start();
    if(isset($_POST['storyname']) && isset($_POST['comment'])){
      
       $story_name = isset($_POST['storyname']) ? filter_input(INPUT_POST, 'storyname', FILTER_SANITIZE_STRING) : '';
       $comment = isset($_POST['comment']) ? filter_input(INPUT_POST, 'comment', FILTER_SANITIZE_STRING) : '';
       $username = $_SESSION['username'];

       if($_SESSION['token'] !== $_POST['token']){
             die("Request forgery detected");
        } 
       require 'database.php';


       
       $stmt1 = $mysqli->prepare("SELECT id FROM stories WHERE name = ? ");
       if(!$stmt1){
         printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }
       $stmt1->bind_param('s', $story_name);
       $stmt1->execute();
       $stmt1->bind_result($id);
       $stmt1->fetch();
       $stmt1->close();

       $stmt2 = $mysqli->prepare("INSERT INTO comments (story_id, story_name, comments, user ) VALUES (?,?,?,?)");
       if(!$stmt2){
         printf("Query Prep Failed: %s\n", $mysqli->error);
             exit;
       }
       $stmt2->bind_param('ssss', $id,$story_name,$comment,$username);
       $stmt2->execute();
       $stmt2->bind_result($id);
       $stmt2->close();



       header("Location: stories.php");
       exit;
      }

?>