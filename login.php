<?php
   session_start();
?>
<!DOCTYPE HTML>
<html>
   <head>
        <title> News Site </title>
   </head>
   <body>
        <?php
            echo "<h1>Enter your username</h1>"
        ?>
         <form action= "<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
          <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            <p>
               <input type="text" name="username" />
               <input type="password" name="password" />
            </p>
            <p>
               <button type="submit" value="Log In">Log In</button>
            </p>
         </form>
         <form action="register.php" method="GET">
            <p>
                  <button type="submit" value="Submit">Register Here</button>
            </p>
        </form>
         <?php
            $username = isset($_POST['username']) ? filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING) : '';
            $password = isset($_POST['password']) ? filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING) : '';
            $username = trim($username);
            $password = trim($password);
            if(isset($_POST['username']) && isset($_POST['password'])) {               
               require 'database.php';              
 
               $stmt = $mysqli->prepare("SELECT COUNT(*), id, password FROM user_information WHERE username=?");
               if(!$stmt){
                     printf("Query Prep Failed: %s\n", $mysqli->error);
                     exit;
               }
               $stmt->bind_param('s', $username);
               $stmt->execute();
               $stmt->bind_result($cnt, $user_id, $pwd_hash);
               $stmt->fetch();
               $stmt->close();
               
               if($cnt === 1 && crypt($password, $pwd_hash)===$pwd_hash && $username != "") {
                       $_SESSION['user_id'] = $user_id;
                       $_SESSION['username'] = $username;
                       $_SESSION['token'] = substr(md5(rand()), 0, 10);
                       header("Location: stories.php");
                       exit;
               }
               else {
                        echo "Your username or password is incorrect.";
               }              
              
            }
         ?>
   </body>
</html>