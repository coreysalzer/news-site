<?php
   session_start();
   ?>
<!DOCTYPE HTML>


<html>
   <head>
        <title> News Site </title>
   </head>
   <body>   
            <?php
              echo " <div id=\"heading\"> ";
               echo " <h1> The Best News Site Ever: </h1>";
               echo " </div>";
             ?>
         <div id="stories">
            <style>
               #stories {
                  float: left;
                  width: 50%;
                  padding-right: 10px;
                  border-right-style: solid;
               }
               
               #heading {
                  padding-top: 2px;
                  border-bottom-style: solid;
               }
               
               h1, h3 {
                  text-align: center;
               }              
            </style>

            <h3> Stories: </h3>

            <?php
            if(!isset($_SESSION['username'])){
               $userIsRegistered = true;
            }
            
            require 'database.php';
            
            $stmt = $mysqli->prepare("SELECT stories.id, stories.name, link, commentary, categories.name, date_posted, username FROM stories
                                     JOIN categories on (categories.id = category_id) ORDER BY date_posted desc");
            if(!$stmt){
               printf("Query Prep Failed: %s\n", $mysqli->error);
               exit;
            }
            
            $stmt->execute();
            $stmt->bind_result($id, $name, $link, $commentary, $category, $date_posted, $first_comment);
            $stmt->store_result();
            

            $stmt2 = $mysqli->prepare("SELECT comments, date_posted, user FROM comments
                                       
                                       WHERE story_id = ?
                                       ORDER BY date_posted desc");
            if(!$stmt2){
               printf("Query Prep Failed: %s\n", $mysqli->error);
               exit;
            }
 

            
            echo "<ul>";
            while($stmt->fetch()) {

               $stmt2->bind_param('s', $id);
               $stmt2->execute();
               $stmt2->bind_result($user_comment, $date_commented_posted, $user);
               $stmt2->store_result();

               echo "<li>";
               
               //Displays the name as a link if the story has a link
               if($link != NULL) {
                  echo "<a href=" . $link . ">" . $name . "</a><br />";
               }
               else {
                  echo "<a>" . $name . "</a><br />";
               }

               echo "<label>Uploaded: </label><a> " . $date_posted . "</a><br />";

               //Displayes the commentary if the story has it
               if($commentary != NULL) {
                  echo "<label>Commentary: </label><a> " . $commentary . "</a><br />";
               }

               while($stmt2->fetch()) {
                  if($user_comment != NULL) {
                  echo "<li>" .  $user . "<label> Commented: </label><a> " . $user_comment . "</a></li>";
                  }
               }




               echo "<label>Category: </label><a> " . $category . "</a><br />";
               
               echo "</li>";
               echo "<br>";



            }
           
         ?>
            


         </div>
         <div id="upload">
            <style>
               #upload {
                  float: right;
                  width: 45%;
               }
            </style>
            <?php
               if($_SESSION['username'] != "") {
            ?>
                  <form enctype="multipart/form-data" action="upload.php" method="POST">
                     <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                     <p>
                        <label for="story_name">Choose a story to submit:</label><br />
                        <label for="story_name">Name:</label>
                        <input name="storyname" type="text" id="story_name" />
                     </p>
                     <p>
                        <input type="submit" value="Submit Story" /><br><br>
                     </p>
                  </form>
                  
                  <form action="delete.php" method="POST">                        
                        <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                        <?php
                           $username = $_SESSION['username']; 
                           require 'database.php';
                           
                           $stmtid = $mysqli->prepare("SELECT id FROM user_information WHERE username=?");
                           if(!$stmtid){
                              printf("Query Prep Failed: %s\n", $mysqli->error);
                              exit;
                           }
                           $stmtid->bind_param('s', $username);
                           $stmtid->execute();
                           $stmtid->bind_result($user_id);
                           $stmtid->fetch();
                           $stmtid->close();
                           
                           $stmt = $mysqli->prepare("SELECT name FROM stories WHERE user_id=?");
                           if(!$stmt){
                                 printf("Query Prep Failed: %s\n", $mysqli->error);
                                 exit;
                           }
                           $stmt->bind_param('i', $user_id);
                           $stmt->execute();
                           $stmt->bind_result($story_name);
                           $stmt->store_result();
                           echo $story_name;
                           
                           echo "Delete Story : <select class = \"delete\" name=\"storytodelete\">";
                           while($stmt->fetch()) {
                               echo "<option value = \"" . $story_name . "\">" . $story_name . "</option>";
                           }
                           $stmt->close();
                       ?>
                        </select>
                     
                     <button type="submit" value="Submit">Delete Story</button><br />
                     <style>
                           .delete option {
                              height: 50px;
                           }
                     </style>
                  </form>

                  <form action="comment.php" method="POST">
                     <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                     
                     <br> <br />
                     <?php
                           $username = $_SESSION['username']; 
                           require 'database.php';
                           
                           
                           $stmt = $mysqli->prepare("SELECT name FROM stories");
                           if(!$stmt){
                                 printf("Query Prep Failed: %s\n", $mysqli->error);
                                 exit;
                           }
               
                           $stmt->execute();
                           $stmt->bind_result($story_name);
                           $stmt->store_result();
                           
                           
                           echo "Story to Comment on: <select class = \"delete\" name=\"storyname\">";
                           while($stmt->fetch()) {
                               echo "<option value = \"" . $story_name . "\">" . $story_name . "</option>";
                           }
                           $stmt->close();
                       ?>
                     </select>

                     <p>
         

                        <label for="comment">Comment:</label>
                        <input name="comment" type="text" id="comments" />
                     </p>
                     <p>
                        <button type="submit" value="Submit">Add Comment</button>
                     </p>
                  </form>

                  <form action="edit_story.php" method="POST">
                     <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                        <label for="storyname">Choose story to edit:</label><br />
                         <?php
                           $username = $_SESSION['username']; 
                           require 'database.php';
                           
                           $stmtid = $mysqli->prepare("SELECT id FROM user_information WHERE username=?");
                           if(!$stmtid){
                              printf("Query Prep Failed: %s\n", $mysqli->error);
                              exit;
                           }
                           $stmtid->bind_param('s', $username);
                           $stmtid->execute();
                           $stmtid->bind_result($user_id);
                           $stmtid->fetch();
                           $stmtid->close();
                           
                           $stmt = $mysqli->prepare("SELECT name FROM stories WHERE user_id=?");
                           if(!$stmt){
                                 printf("Query Prep Failed: %s\n", $mysqli->error);
                                 exit;
                           }
                           $stmt->bind_param('i', $user_id);
                           $stmt->execute();
                           $stmt->bind_result($story_name);
                           $stmt->store_result();
                           
                           
                           echo "Story to Edit : <select class = \"delete\" name=\"storyname\">";
                           while($stmt->fetch()) {
                               echo "<option value = \"" . $story_name . "\">" . $story_name . "</option>";
                           }
                           $stmt->close();
                       ?>
                        </select>
                     <p>

                       
                        <label for="storynameedit">Story Name Edit:</label>
                        <input name="storyname_edit" type="text" id="storynameedit" /><br />
                        <label for="links">Story Link Edit:</label>
                        <input name="link" type="text" id="links" /><br />
                        <label for="comments">Story Comment Edit:</label>
                        <input name="comment" type="text" id="comments" /><br />
                     </p>
                     <p>
                        <button type="submit" value="Submit">Edit</button>
                     </p>
                  </form>


                  <form action="delete_commentary.php" method="POST">
                     <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                        <label for="storyname">Choose story to remove commentary from:</label><br />
                         <?php
                           $username = $_SESSION['username']; 
                           require 'database.php';
                           
                           $stmtid = $mysqli->prepare("SELECT id FROM user_information WHERE username=?");
                           if(!$stmtid){
                              printf("Query Prep Failed: %s\n", $mysqli->error);
                              exit;
                           }
                           $stmtid->bind_param('s', $username);
                           $stmtid->execute();
                           $stmtid->bind_result($user_id);
                           $stmtid->fetch();
                           $stmtid->close();
                           
                           $stmt = $mysqli->prepare("SELECT name FROM stories WHERE user_id=?");
                           if(!$stmt){
                                 printf("Query Prep Failed: %s\n", $mysqli->error);
                                 exit;
                           }
                           $stmt->bind_param('i', $user_id);
                           $stmt->execute();
                           $stmt->bind_result($story_name);
                           $stmt->store_result();
                           
                           
                           echo "Story to Edit : <select class = \"delete\" name=\"storyname\">";
                           while($stmt->fetch()) {
                               echo "<option value = \"" . $story_name . "\">" . $story_name . "</option>";
                           }
                           $stmt->close();
                       ?>
                         </select>
                     <p>
                        <button type="submit" value="Submit">Remove</button>
                     </p>
                  </form>
                  
                  
                  
                  <form action="edit_comments.php" method="POST">
                     <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                        <label for="storyname">Choose story to edit comments for:</label><br />
                         <?php
                           $username = $_SESSION['username']; 
                           require 'database.php';
                           
               
                              $stmt = $mysqli->prepare("SELECT comment_id, comments, story_name FROM comments WHERE user=? ");
                           if(!$stmt){
                                 printf("Query Prep Failed: %s\n", $mysqli->error);
                                 exit;
                           }
                           $stmt->bind_param('s', $username);
                           $stmt->execute();
                           $stmt->bind_result($id,$comment,$story_name);
                           $stmt->store_result();
                           
                           
                              echo "Comment to Edit : <select class = \"edit\" name=\"id\">";
                              while($stmt->fetch()) {
                                  echo "<option value = \"" . $id . "\">" . $story_name. ": " . $comment . "</option>";
                              }
                              $stmt->close();
                              
                           
                       ?>
                        </select>

                     <p>
                        <label for="changed_comment">Story Comment Edit:</label>
                        <input name="changed_comment" type="text" id="comments" /><br />
                     </p>
                     <p>
                        <button type="submit" value="Submit">Edit</button>
                     </p>
                  </form>
                  
                   <form action="delete_comment.php" method="POST">
                     <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
                        <label for="storyname">Choose comment to delete:</label><br />
                         <?php
                           $username = $_SESSION['username']; 
                           require 'database.php';
                           
               
                              $stmt = $mysqli->prepare("SELECT comment_id, comments, story_name FROM comments WHERE user=? ");
                           if(!$stmt){
                                 printf("Query Prep Failed: %s\n", $mysqli->error);
                                 exit;
                           }
                           $stmt->bind_param('s', $username);
                           $stmt->execute();
                           $stmt->bind_result($id,$comment,$story_name);
                           $stmt->store_result();
                           
                           
                              echo "Comment to Delete : <select class = \"edit\" name=\"id\">";
                              while($stmt->fetch()) {
                                  echo "<option value = \"" . $id . "\">" . $story_name. ": " . $comment . "</option>";
                              }
                              $stmt->close();
                              
                           
                       ?>
                        </select>
                        
                     
                        <p>
                        
                           <input type="hidden" name="delete_id" value= '$id' />
                           <button type="submit" value="Submit">Delete Comment</button>
                        </p>
                     
                     <p>
                  </form>
                  


                  <form action="logoff.php" method="GET">
                     <p>
                           <button type="submit" value="Submit">Log Off</button>
                     </p>
                  </form>
            <?php
               }
               else {
                  ?>
                     <form action="login.php" method="GET">
                        <p>
                              <button type="submit" value="Submit">Log In</button>
                        </p>
                     </form>
                  
                  <?php
               }
            ?>
            
            
            
            
   
           
         </div>
         

   </body>
</html>