<?php
    session_start();
    $story_name= isset($_POST['storyname']);
?>
<!DOCTYPE HTML>
<html>
   <head>
        <title> Upload Story </title>
   </head>
   <body>
        <?php
            echo "<h1>Upload Story</h1>"
        ?>
         <form action= "<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            <p>
               Name: <input type="text" name="name" value="<?php echo $_POST['storyname'] ?>" /><br />
               Link: <input type="text" name="link" /><br />
               Commentary: <input type="text" name="commentary" /><br />
               Category: <select name="category">
                <?php
                    require 'database.php';
                    $stmt = $mysqli->prepare("SELECT name FROM categories");
                    if(!$stmt){
                          printf("Query Prep Failed: %s\n", $mysqli->error);
                          exit;
                    }
                    $stmt->execute();
                    $stmt->bind_result($category_name);
                    while($stmt->fetch()) {
                        echo "<option value = \"" . $category_name . "\">" . $category_name . "</option>";
                    }
                    $stmt->close();
                ?>
               </select><br />
            </p>
            <p>
               <button type = "submit" value="Upload">Upload Story</button>
            </p>
         </form>
        <form action="stories.php" >
            <p>
                  <button type="submit" value="Submit">Go Back To Stories</button>
            </p>
        </form>
        <?php
            
            $username = $_SESSION['username'];
            $name = isset($_POST['name']) ? filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING) : '';
            $link = isset($_POST['link']) ? filter_input(INPUT_POST, 'link', FILTER_SANITIZE_STRING) : '';
            $commentary = isset($_POST['commentary']) ? filter_input(INPUT_POST, 'commentary', FILTER_SANITIZE_STRING) : '';
            $category = $_POST['category'];            
            if($_SESSION['token'] !== $_POST['token']){
                die("Request forgery detected");
            }

            if(isset($_POST['name']) && isset($_POST['category'])) {
                require 'database.php';              

                $stmt1 = $mysqli->prepare("SELECT COUNT(*) FROM stories WHERE name=?");
                if(!$stmt1){
                      printf("Query Prep Failed: %s\n", $mysqli->error);
                      exit;
                }
                $stmt1->bind_param('s', $name);
                $stmt1->execute();
                $stmt1->bind_result($cnt);
                $stmt1->fetch();
                $stmt1->close();
                
                if($cnt === 0) {
                    $stmt2 = $mysqli->prepare("SELECT id FROM user_information WHERE username=?");
                     if(!$stmt2){
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        exit;
                     }
                     $stmt2->bind_param('s', $username);
                     $stmt2->execute();
                     $stmt2->bind_result($user_id);
                     $stmt2->fetch();
                     $stmt2->close();
                     
                     $stmt3 = $mysqli->prepare("SELECT id FROM categories WHERE name=?");
                     if(!$stmt3){
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        exit;
                     }
                     $stmt3->bind_param('s', $category);
                     $stmt3->execute();
                     $stmt3->bind_result($category_id);
                     $stmt3->fetch();
                     $stmt3->close();
                    
                    $stmt4 = $mysqli->prepare("INSERT INTO stories (name, link, commentary, category_id, user_id, username) VALUE (?, ?, ?, ?, ?, ?)");
                     if(!$stmt4){
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        exit;
                     }
                     $stmt4->bind_param('ssssss', $name, $link, $commentary, $category_id, $user_id,$username);
                     $stmt4->execute();
                     header("Location: stories.php");
                     $stm4->close();
                     exit;
                }
                else {
                    echo "A story with that name already exists.";
                    exit;
                }
                exit;
            }
        ?>
   </body>
</html>
