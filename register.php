<?php
   session_start();
?>
<!DOCTYPE HTML>
<html>
   <head>
        <title> Registration </title>
   </head>
   <body>
        <?php
            echo "<h1>Register your username</h1>"
        ?>
         <form action= "<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            <p>
               <input type="text" name="username" />
               <input type="text" name="password" />
            </p>
            <p>
               <button type="submit" value="Log In">Register</button>
            </p>
         </form>
        <form action="login.php" >
            <p>
                  <button type="submit" value="Submit">Go Back To Login</button>
            </p>
        </form>
         <?php
            $username = isset($_POST['username']) ? filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING) : '';
            $password = isset($_POST['password']) ? filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING) : '';
            if(isset($_POST['username']) && isset($_POST['password'])) {
               
               require 'database.php';
                
               $pwd_hash= crypt($password);
               
               if($username != "" and $password != "") {
                  $stmt1 = $mysqli->prepare("SELECT COUNT(*) FROM user_information WHERE username=?");
                  if(!$stmt1){
                     printf("Query Prep Failed: %s\n", $mysqli->error);
                     exit;
                  }
                  
                  $stmt1->bind_param('s', $username);
                  $stmt1->execute();
                  $stmt1->bind_result($cnt);
                  $stmt1->store_result();
                  $stmt1->fetch();
               
                  if($cnt === 0) {
                     $stmt2 = $mysqli->prepare("INSERT INTO user_information (username, password) VALUE (?, ?)");
                     if(!$stmt2){
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        exit;
                     }
                     $stmt2->bind_param('ss', $username, $pwd_hash);
                     $stmt2->execute();
                  }
                  else {
                     echo "The username you entered has already been chosen. Please enter a different one.";
                     exit;
                  }
               }
               else {
                  echo "Your username or password is incorrect.";
                  exit;
               }
              
               echo ("You are now registered!");
               exit;
            }
         ?>

   </body>
</html>

